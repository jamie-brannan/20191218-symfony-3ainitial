<?php

class Stormtrooper
{
    private $name = '';

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function __construct(string $name)
    {
        echo "My name is ".$name.PHP_EOL;
        $this->name = $name;
    }

    public function __destruct()
    {
        echo "Bam bam they shot me down".PHP_EOL;
    }

    public function __call($name, $arguments)
    {
        echo "You called $name, too bad".PHP_EOL;
    }
}
